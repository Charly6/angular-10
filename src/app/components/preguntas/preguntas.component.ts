import { Component, OnInit } from '@angular/core';
import dataQuestions from '../../../assets/json/datos.json'

@Component({
  selector: 'app-preguntas',
  templateUrl: './preguntas.component.html',
  styleUrls: ['./preguntas.component.css']
})
export class PreguntasComponent implements OnInit {

  questions = dataQuestions;
  answers = [
    "",
    "",
    "",
    "",
    "",
    false
  ]
  mensaje = false

  constructor() {
    console.log(this.questions);
  }

  ngOnInit(): void {
  }

  guardar() {
    console.log(this.answers);
    if (!this.answers[5]) {
      this.mensaje = true
    }else{
      console.log('guardando..');
      this.mensaje = false
    }
  }

  cambio() {
    this.mensaje=false
  }
}
